# -*- mode: python; python-indent: 4 -*-
import ncs
from ncs.application import Service


# ------------------------
# SERVICE CALLBACK EXAMPLE
# ------------------------
class ServiceCallbacks(Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info('Service create(service=', service._path, ')')

        vars = ncs.template.Variables()
        vars.add('DEVICE', service._parent._parent.name)
        vars.add('CR', 'true')
        service.vars.interface_name = service.name
        template = ncs.template.Template(service)
        template.apply('backbone-interface', vars)


class Main(ncs.application.Application):
    def setup(self):
        self.register_service('netinfra-rfs-backbone-interface-servicepoint', ServiceCallbacks)

    def teardown(self):
        pass
